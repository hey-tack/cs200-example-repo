#include <iostream>
#include <string>
using namespace std;

// These comments are being added for the 
// source control Tech Literacy Lab. Hi!

// While loops lab 1: Count Up
void Program1()
{
    int count;
    count = 0;
    while (count <= 20)  {
        cout << count << " ";
        count++;
    }
}

// While loops lab 2: Multiply up
void Program2()
{
    int count;
    count = 1;
    while (count <= 128) {
        cout << count << " ";
        count *= 2;
    }
}

// While loops lab 3: Number guesser
void Program3()
{
    int secretNumber = 16;
    int playerGuess;

    do {
        cout << "Guess a number: ";
        cin >> playerGuess;
        if (playerGuess > secretNumber) {
            cout << "Too high!" << endl;
        } else if (playerGuess < secretNumber) {
            cout << "Too low!" << endl;
        } else {
            cout << "That's right!" << endl;
        }
    }
    while (playerGuess != secretNumber);

    cout << "GAME OVER" << endl;
}

// While loops lab 4: Input validator
void Program4()
{
    int userInput;
    int minValue = 1;
    int maxValue = 5;

    cout << "Please enter a number between 1 and 5: ";
    cin >> userInput;

    while (userInput < minValue || userInput > maxValue) {
        cout << "Invalid input, try again: ";
        cin >> userInput;
    }

    cout << "Thank you";
}

// While loops lab 5: Getting a raise
void Program5()
{
    float startingWage;
    float percentRaisePerYear;
    float adjustedWage;
    int yearsWorked;
    int yearCounter = 1;

    cout << "Enter your starting wage: ";
    cin >> startingWage;
    cout << endl;

    cout << "Enter your percent raise per year: ";
    cin >> percentRaisePerYear;
    cout << endl;

    cout << "Enter your years worked: ";
    cin >> yearsWorked;
    cout << endl;

    // Calculate stuff
    adjustedWage = startingWage;

    while (yearCounter <= yearsWorked) {
        cout << "Salary at year " << yearCounter << ": " << adjustedWage << endl;
        adjustedWage = (adjustedWage * percentRaisePerYear / 100) + adjustedWage;
        yearCounter++;
    }
}

// While loop lab 6: Summation
void Program6()
{
    int n;
    int counter = 1;
    int sum = 0;

    cout << "Please enter a value for n: ";
    cin >> n;
    cout << endl;

    while (counter <= n) {
        sum += counter;
        counter++;
        cout << "Sum: " << sum << endl;
    }
}

int main()
{
    // Don't modify main
    while ( true )
    {
        cout << "Run which program? (1-6): ";
        int choice;
        cin >> choice;

        cout << endl << endl;

        if      ( choice == 1 ) { Program1(); }
        else if ( choice == 2 ) { Program2(); }
        else if ( choice == 3 ) { Program3(); }
        else if ( choice == 4 ) { Program4(); }
        else if ( choice == 5 ) { Program5(); }
        else if ( choice == 6 ) { Program6(); }

        cout << endl << "------------------------------------" << endl;
    }

    return 0;
}
